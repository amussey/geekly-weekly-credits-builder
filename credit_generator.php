<?php
require("settings.php");


if (!isset($current_time)) {
    die("Current time not set.");
}
if (!isset($_POST['credits'])) {
    die("No credits to list off.");
}



$credits = $_POST['credits']; //file_get_contents("credits.txt");
$credits = explode("[CAST]", $credits);


// Clear any spare spaces or linebreaks
for ($i = 0; $i < count($credits); $i++) {
    $credits[$i] = trim($credits[$i]);
}

// Check to see if both sections are there.
if (count($credits) != 2) {
    die("The cast or crew is missing from the credits.");
}

$final_credits = array();

$crew = explode("\n", $credits[0]);
$cast = explode("\n", $credits[1]);


// Set up the crew
for ($i = 0; $i < count($crew); $i++) {
    $crew[$i] = explode("=", $crew[$i]);
    for ($j = 0; $j < count($crew[$i]); $j++) {
        $crew[$i][$j] = trim($crew[$i][$j]);
    }
    $crew[$i][0] = strtolower($crew[$i][0]);
    $crew[$i][1] = strtoupper($crew[$i][1]);
}

for ($i = 0; $i < count($crew); $i++) {
    for ($j = $i+1; $j < count($crew); $j++) {
        if ($crew[$i][0] == $crew[$j][0]) {
            $crew[$j][0] = "";
        }
    }
}

// Set up the cast
for ($i = 0; $i < count($cast); $i++) {
    $cast[$i] = explode("=", $cast[$i]);
    for ($j = 0; $j < count($cast[$i]); $j++) {
        $cast[$i][$j] = trim($cast[$i][$j]);
    }
    $cast[$i][1] = strtoupper($cast[$i][1]);
}

if (count($crew) < 8 && count($cast) < 7) {
    // Single panel for each side
    $final_credits[0] = $crew;
    array_unshift($cast, array("CAST", ""));
    $final_credits[1] = $cast;
} else if (count($crew) < 8 && count($cast) < 19){
    // Single panel for crew, 3 panel for cast
    $final_credits[0] = $crew;
    $final_credits[1] = array_slice($cast, 0, 6);
    array_unshift($final_credits[1], array("CAST", ""));
    $final_credits[2] = array_slice($cast, 6, 12);
    array_unshift($final_credits[2], array("CAST", ""));
    //$final_credits[3] = array_slice($cast, 0, 6);
    
} else { //(count($crew) < 15 && count($cast) < 7)
    //
    die("These situations still need to be filled in.");
}



for ($k = 0; $k < count($final_credits); $k++) {
    generate_credits($final_credits[$k], "temp/".$current_time."/credits_text_".$k.".png");
}

$current_k = 1;
for ($k = 0; $k < count($final_credits); $k+=2) {
    $base_img = imagecreatefrompng("images/credits_blank.png");
    if (isset($final_credits[$k+1])) {
        // both exist
        $casta = imagecreatefrompng("temp/".$current_time."/credits_text_".$k.".png");
        $castb = imagecreatefrompng("temp/".$current_time."/credits_text_".($k+1).".png");
        imagecopymerge($base_img, $casta, 50,  654, 0, 0, 885, 376, 100);
        imagecopymerge($base_img, $castb, 985, 654, 0, 0, 885, 376, 100);
        imagedestroy($casta);
        imagedestroy($castb);
        unlink("temp/".$current_time."/credits_text_".$k.".png");
        unlink("temp/".$current_time."/credits_text_".($k+1).".png");
    } else {
        // only one exists
        $casta = imagecreatefrompng("temp/".$current_time."/credits_text_".$k.".png");
        imagecopymerge($base_img, $casta, 50,  654, 0, 0, 885, 376, 100);
        imagedestroy($casta);
        unlink("temp/".$current_time."/credits_text_".$k.".png");
    }
    imagepng($base_img, "temp/".$current_time."/credits_image_".$current_k.".png");
    $current_k++;

    imagedestroy($base_img);
}
$last_value = ceil(count($final_credits)/2.0)+1;
copy("images/credits_copyright.png", "temp/".$current_time."/credits_image_".$last_value.".png");



$total_credits_pages = $last_value;

// print_r($cast);
// echo "\n\n";
// print_r($crew);

// echo "\n\n";




// print_r($credits);

function generate_credits($credit_array, $output_file) {
    global $font;
    global $fontb;

    // Create the image
    $im = imagecreatetruecolor(885, 376);

    // Create some colors
    $white = imagecolorallocate($im, 255, 255, 255);
    $grey = imagecolorallocate($im, 128, 128, 128);
    $black = imagecolorallocate($im, 0, 0, 0);
    imagefilledrectangle($im, 0, 0, 399, 29, $black);

    // Replace path by your own font path
    $font_size = 35;


    // Add the text
    for ($i = 0; $i < count($credit_array); $i++) {
        imagettftext($im, $font_size, 0, 2, ($font_size+$i*52), $white, $font, $credit_array[$i][0]);
    }

    for ($i = 0; $i < count($credit_array); $i++) {
        $dimensions = imagettfbbox($font_size, 0, $fontb, $credit_array[$i][1]);
        $textWidth = abs($dimensions[4] - $dimensions[0]);
        $x = imagesx($im) - $textWidth;
        imagettftext($im, $font_size, 0, $x, ($font_size+$i*52), $white, $fontb, $credit_array[$i][1]);
    }

    imagepng($im, $output_file);
    imagedestroy($im);
}