#!/bin/bash

# Iterate through and convert all of the images to videos
let END=$3 i=1
ALL_VIDEOS=""
while ((i<=END)); do
    echo $i
    ffmpeg -loop 1 -i "temp/$1/credits_image_$i.png" -c:v libx264 -t $4 -pix_fmt yuv420p "temp/$1/credits_image_$i.mp4"
    ffmpeg -i "temp/$1/credits_image_$i.mp4" -c copy -bsf:v h264_mp4toannexb -f mpegts "temp/$1/intermediate_$i.ts"
    if [[ $i -eq 1 ]]; then
        ALL_VIDEOS="temp/$1/intermediate_$i.ts"
    else
        ALL_VIDEOS="$ALL_VIDEOS|temp/$1/intermediate_$i.ts"
    fi
    let i++
done

ffmpeg -i "concat:$ALL_VIDEOS" -c copy -bsf:a aac_adtstoasc "temp/$1/full_credits.mp4"

#Strip the audio from the two videos
ffmpeg -i "stock_videos/$6" -an -c:v libx264 -t 00:00:$5 -pix_fmt yuv420p -ss 00:00:35 temp/$1/top_video.mp4
ffmpeg -i "stock_videos/$7" -an -c:v libx264 -t 00:00:$5 -pix_fmt yuv420p -ss 00:00:35 temp/$1/bottom_video.mp4




#merge them in

# DO NOT remove the log levels.  These are kept in intentionally to slow
# down ffmpeg.  Otherwise, it will consume all of the system RAM and die.
ffmpeg -i "temp/$1/full_credits.mp4" -i "$2" -i "temp/$1/top_video.mp4" -i "temp/$1/bottom_video.mp4" \
    -filter_complex "
        nullsrc=size=1920x1080 [base];
        [0:v] setpts=PTS-STARTPTS, scale=1920x1080 [upperleft];
        [1:v] setpts=PTS-STARTPTS, scale=854x479 [upperright];
        [2:v] setpts=PTS-STARTPTS, scale=385x218 [topside];
        [3:v] setpts=PTS-STARTPTS, scale=385x218 [bottomside];
        [base][upperleft] overlay=shortest=1 [tmp1];
        [tmp1][upperright] overlay=shortest=1:x=574:y=56 [tmp2];
        [tmp2][topside] overlay=shortest=1:x=1485:y=50 [tmp3];
        [tmp3][bottomside] overlay=shortest=1:x=1485:y=323 
    " -c:v libx264  temp/$1/final_render.mp4 -v 9 -loglevel 99

rm "temp/$1/top_video.mp4"
rm "temp/$1/bottom_video.mp4"

let END=$3 i=1
ALL_VIDEOS=""
while ((i<=END)); do
    rm "temp/$1/credits_image_$i.png"
    rm "temp/$1/credits_image_$i.mp4"
    rm "temp/$1/intermediate_$i.ts"
    let i++
done

mv "temp/$1/final_render.mp4" "output/$1.mp4"
my_ip=`curl ipecho.net/plain`
rm temp/$1/center*
rm "temp/$1/full_credits.mp4"
rmdir temp/$1/

php send_email.php "$8" "http://$my_ip/credits/output/$1.mp4"
