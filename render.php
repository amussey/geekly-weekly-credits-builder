<?php
if (!isset($_POST)) {
    header("Location: index.php");
    exit;
}

if (!isset($_POST['email_address'])) {
    die("Please enter your email address.");
}

ini_set('display_errors', 'On');
error_reporting(E_ALL);



// Use the current time to create a folder.
$current_time = time();
mkdir("temp/".$current_time);

// Render the credits
include "credit_generator.php";


//Check for the uploaded video
$extension = explode(".", basename($_FILES['center_video']['name']));
$center_video = "temp/".$current_time."/center.".$extension[count($extension)-1];

//$center_video = $center_video.basename($_FILES['center_video']['name']);

if(move_uploaded_file($_FILES['center_video']['tmp_name'], $center_video)) {
    //echo "The file ".  basename( $_FILES['center_video']['name'])." has been uploaded";
} else {
    print_r($_FILES);
    die("There was an error uploading and moving the file.");
}

$center_video_movie = new ffmpeg_movie($center_video);

$frame_duration = $center_video_movie->getDuration()/floatval($total_credits_pages);
//die("<br />Done! (".$center_video_movie->getDuration()." -- ".$frame_duration.") ".$center_video);





// Get the number of credit files


// Alright, now, render the background


// Let's combine the top files

$cmd = "/var/www/credits/run_render.sh ".
    $current_time." ".
    $center_video." ".
    $total_credits_pages." ".
    $frame_duration." ".
    $center_video_movie->getDuration()." \"".
    $_POST['top_video']."\" \"".
    $_POST['bottom_video']."\" \"".$_POST['email_address']."\"";
$outputfile = "/var/www/credits/out_test_log.txt";
$pidfile = "/var/www/credits/out_test_pid.txt";
exec(sprintf("%s > %s 2>&1 & echo $! >> %s", $cmd, $outputfile, $pidfile));
//print_r($_POST);


?>
Your credit sequence is being rendered right now!<br />
You will get an email with a download link in your inbox as soon as it's ready!<br />

