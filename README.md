# Credits Builder for The Sketch

![Credits Demo Image](https://bitbucket.org/amussey/vttv-the-sketch-credits-builder/raw/master/images/credits_demo.png)

To get started with this script, run the `setup.sh` script to create the necessary directories, fill in `settings.template.php` and make a copy of it as `settings.php`, and populate `stock_videos/videos.txt`.
