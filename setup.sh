#!/bin/bash

if [ $UID != 0 ] ;
then
    echo "Please run this script as root."
fi

# Setup the environment so this script can run.

mkdir temp
chmod 2777 temp

mkdir output
chmod 2777 output

# Begin the compile and install of ffmpeg.
# These instructions have been tested on Ubuntu 13.04.
apt-get update
apt-get install vim autoconf automake make gcc build-essential git libass-dev libgpac-dev   libsdl1.2-dev libtheora-dev libtool libva-dev libvdpau-dev libvorbis-dev libx11-dev  libxext-dev libxfixes-dev pkg-config texi2html zlib1g-dev apache2 libvpx-dev tree -y 
apt-get install yasm php5 libmp3lame-dev -y 

# Install x264
mkdir ~/ffmpeg_sources
cd ~/ffmpeg_sources
git clone --depth 1 git://git.videolan.org/x264.git x264
cd x264
./configure --prefix="$HOME/ffmpeg_build" --bindir="$HOME/bin" --enable-static
make
make install
make distclean
cd ~

# Install AAC
cd ~/ffmpeg_sources
git clone --depth 1 git://github.com/mstorsjo/fdk-aac.git
cd fdk-aac
autoreconf -fiv
./configure --prefix="$HOME/ffmpeg_build" --disable-shared
make
make install
make distclean
cd ~

# Install opus
cd ~/ffmpeg_sources
wget http://downloads.xiph.org/releases/opus/opus-1.0.3.tar.gz
tar xzvf opus-1.0.3.tar.gz
cd opus-1.0.3
./configure --prefix="$HOME/ffmpeg_build" --disable-shared
make
make install
make distclean
cd ~

# Install ffmpeg
cd ~/ffmpeg_sources
git clone --depth 1 git://source.ffmpeg.org/ffmpeg
cd ffmpeg
PKG_CONFIG_PATH="$HOME/ffmpeg_build/lib/pkgconfig"
export PKG_CONFIG_PATH
./configure --prefix="$HOME/ffmpeg_build"   --extra-cflags="-I$HOME/ffmpeg_build/include" --extra-ldflags="-L$HOME/ffmpeg_build/lib"   --bindir="$HOME/bin" --extra-libs="-ldl" --enable-gpl --enable-libass --enable-libfdk-aac   --enable-libmp3lame --enable-libopus --enable-libtheora --enable-libvorbis --enable-libvpx   --enable-libx264 --enable-nonfree --enable-x11grab
make
make install
make distclean

# Install the final ffmpeg libraries
apt-get install php5-ffmpeg php5-gd -y

echo 'export PATH=$PATH:~/bin/' >> .bashrc
